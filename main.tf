terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

resource "aws_instance" "MyFirstTest" {
  ami           = "ami-0915bcb5fa77e4892"
  #ami              = "ami-042e8287309f5df03"
  instance_type = "t2.micro"

  tags = {
   # Name = "MyfirstTerraformInstance"
     Name = var.instance_name
  }
}